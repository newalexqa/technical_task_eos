import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

from preferences import Preferences


@pytest.fixture(scope="module")
def browser():
    chrome_options = Options()
    chrome_options.add_experimental_option("prefs", Preferences.chrome_pref)
    for arg in Preferences.chrome_args:
        chrome_options.add_argument(arg)
    driver = Chrome(Preferences.chrome_driver_path, options=chrome_options)
    yield driver
    driver.quit()

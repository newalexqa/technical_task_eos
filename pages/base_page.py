from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException
import time as t
import imaplib
import email
import base64
import re

from preferences import Preferences
from routes import Routes


class BasePage(Preferences, Routes):
    def __init__(self, driver):
        self.driver = driver

    def open(self, route, time: int = 0):
        self.driver.get(f"{self.main_url}{route.lstrip('/').strip()}")
        WebDriverWait(self.driver, time if time else self.default_timeout).until(
            lambda x: self.driver.title.strip())

    def find_element(self, locator: str, time: int = 0):
        return WebDriverWait(self.driver, time if time else self.default_timeout).until(
            ec.presence_of_element_located(locator), message=f"NOT FOUND{locator}")

    def find_elements(self, locator: str, time: int = 0):
        return WebDriverWait(self.driver, time if time else self.default_timeout).until(
            ec.presence_of_all_elements_located(locator), message=f"NOT FOUND {locator}")

    def check_element_text(self, locator: str, text: str):
        assert self.find_element(locator).text == text

    def is_error_present(self, locator: str, time: int = 0):
        try:
            WebDriverWait(self.driver, time if time else self.default_timeout).until(
                ec.presence_of_all_elements_located(locator))
            return True
        except TimeoutException:
            return False

    def is_checkbox_checked(self, locator: str, time: int = 0):
        return self.find_element(locator, time).is_selected()

    def wait_for_page_redirect(self, new_route: str):
        for _ in range(self.wait_retry_timeout):
            if new_route in self.driver.current_url:
                return True
            t.sleep(1)
        return False

    def extract_email_body(self, pl):
        if isinstance(pl, str):
            return pl
        else:
            return '\n'.join([self.extract_email_body(part.get_payload()) for part in pl])

    def get_code_from_email(self):
        conn = imaplib.IMAP4_SSL(self.gmail_imap_server, self.gmail_imap_port)
        conn.login(f"{self.email_name}{self.email_domain}", self.email_password)
        for _ in range(self.wait_confirm_email_timeout):
            conn.select()
            typ, data = conn.search(None, '(UNSEEN)')
            for num in data[0].split():
                typ, msg_data = conn.fetch(num, '(RFC822)')
                for response_part in msg_data:
                    if isinstance(response_part, tuple):
                        msg = email.message_from_string(response_part[1].decode("utf-8"))
                        if msg["From"] == self.sender and msg["To"] == self.email:
                            payload = msg.get_payload()
                            body = self.extract_email_body(payload)
                            body = base64.b64decode(body).decode("UTF-8")
                            codes = re.findall(r"(\d{2}-\d{2})", body)
                            if not codes:
                                conn.close()
                                raise Exception("No codes in email")
                            return codes[0]
            t.sleep(1)
        conn.close()

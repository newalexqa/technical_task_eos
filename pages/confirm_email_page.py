from pages.base_page import BasePage
from pages.locators.confirm_email_page_locators import ConfirmEmailPageLocators


class ConfirmEmailPage(BasePage, ConfirmEmailPageLocators):

    def check_displayed_email(self):
        self.check_element_text(self.email_locator, self.email)

    def resend_email_code(self):
        self.find_element(self.resend_email_locator).click()

    def enter_confirm_code(self, code):
        field = self.find_element(self.confirm_code_input_locator)
        field.send_keys(code)
        self.wait_for_page_redirect(self.main_board_route)

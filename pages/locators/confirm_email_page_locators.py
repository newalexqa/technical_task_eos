from selenium.webdriver.common.by import By


class ConfirmEmailPageLocators:
    email_locator = (By.CLASS_NAME, "email")
    resend_email_locator = (By.XPATH, "//button[@data-id='resend-code-btn']")
    confirm_code_input_locator = (By.ID, "confirm-code-input")

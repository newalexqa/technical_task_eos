from selenium.webdriver.common.by import By


class LoginPageLocators:
    first_name_locator = (By.ID, "first_name")
    last_name_locator = (By.ID, "last_name")
    email_locator = (By.ID, "email")
    password_locator = (By.ID, "password")
    email_error_locator = (By.ID, "mat-error-0")
    terms_checkbox_input_locator = (By.CLASS_NAME, "mat-checkbox-inner-container")
    terms_checkbox_locator = (By.ID, "policy_confirm-input")
    submit_locator = (By.XPATH, "//button[@data-id='sign-up-btn']")
    sign_in_locator = (By.XPATH, "//button[@data-id='sign-in-button']")
    sign_in_submit_locator = (By.XPATH, "//button[@data-id='sign-in-btn']")

from selenium.webdriver.common.keys import Keys

from pages.base_page import BasePage
from pages.locators.login_page_locators import LoginPageLocators
from pages.data.login_page_data import LoginPageData
from faker import Faker


class LoginPage(BasePage, LoginPageLocators, LoginPageData):
    fake = Faker()

    def open_login_page(self):
        self.open(self.login_route)
        assert LoginPageData.title == self.driver.title
        assert self.login_route in self.driver.current_url

    def enter_first_name(self, name: str = None):
        field = self.find_element(self.first_name_locator)
        field.clear()
        field.send_keys(name if name else self.fake.first_name())

    def enter_last_name(self, name: str = None):
        field = self.find_element(self.last_name_locator)
        field.clear()
        field.send_keys(name if name else self.fake.last_name())

    def enter_email(self, email: str = None):
        field = self.find_element(self.email_locator)
        field.clear()
        field.send_keys(email if email else self.fake.email())
        field.send_keys(Keys.TAB)
        assert not self.is_error_present(self.email_error_locator, 3), "email validation error"

    def enter_password(self, pwd: str):
        field = self.find_element(self.password_locator)
        field.clear()
        field.send_keys(pwd)

    def apply_terms(self):
        field = self.find_element(self.terms_checkbox_input_locator)
        if not self.is_checkbox_checked(self.terms_checkbox_locator):
            field.click()
        assert self.is_checkbox_checked(self.terms_checkbox_locator), "Terms checkbox not checked"

    def submit_data(self):
        self.find_element(self.submit_locator).click()
        assert self.wait_for_page_redirect(self.confirm_route), "Open new page fail"

    def switch_to_sign_in(self):
        self.find_element(self.sign_in_locator).click()

    def submit_login(self):
        self.find_element(self.sign_in_submit_locator).click()
        self.wait_for_page_redirect(self.main_board_route)

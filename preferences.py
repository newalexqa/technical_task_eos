import uuid
from os.path import join, dirname


class Preferences:
    uid = uuid.uuid4().hex

    # email settings
    main_url = "https://eos.com/crop-monitoring/"
    email_name = "some.tests.email"
    email_domain = "@gmail.com"
    with open(join(dirname(__file__), "email_password.txt"), "r") as f:
        email_password = f.read().strip()
    email = f"{email_name}+{uid}{email_domain}"
    gmail_imap_server = 'imap.gmail.com'
    gmail_imap_port = 993
    sender = "EOS <support@eos.com>"

    # counters and timeouts
    default_timeout = 10  # seconds
    wait_retry_timeout = 60  # seconds
    wait_confirm_email_timeout = 120  # seconds
    resend_email_count = 3  # count

    # paths
    chrome_driver_path = join(dirname(__file__), "drivers", "chromedriver")
    chrome_download_directory = join(dirname(__file__), "downloads")

    # browser settings
    chrome_args = [
        "--no-sandbox",
        # "--headless",
    ]
    chrome_pref = {
        "download.default_directory": chrome_download_directory,
    }
from pages.confirm_email_page import ConfirmEmailPage
from pages.login_page import LoginPage


def test_001_open_login_page(browser):
    login_page = LoginPage(browser)
    login_page.open_login_page()


def test_002_fill_sign_up_form(browser):
    login_page = LoginPage(browser)
    login_page.enter_first_name()
    login_page.enter_last_name()
    login_page.enter_email(login_page.email)
    login_page.enter_password(login_page.uid)
    login_page.apply_terms()
    login_page.submit_data()


def test_003_confirm_email(browser, counter=0):
    confirm_page = ConfirmEmailPage(browser)
    confirm_page.check_displayed_email()
    code = confirm_page.get_code_from_email()
    if not code and counter != confirm_page.resend_email_count:
        confirm_page.resend_email_code()
        return test_003_confirm_email(browser, counter + 1)
    assert code, "Not found email or code in email"
    confirm_page.enter_confirm_code(code)

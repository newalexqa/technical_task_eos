from pages.login_page import LoginPage


def test_001_open_login_page(browser):
    login_page = LoginPage(browser)
    login_page.open_login_page()
    login_page.switch_to_sign_in()


def test_002_fill_sign_in_form(browser):
    login_page = LoginPage(browser)
    login_page.enter_email(login_page.email)
    login_page.enter_password(login_page.uid)
    login_page.submit_login()
